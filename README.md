GO LANGUAGE MAIN TASKS

NORTHWIND DATABASE RESTful API

TASK 1  : GET- Select all Products.

TASK 4  : GET- Select a specific Product entry by ProductID.

TASK 6  : GET- Select all premiumcustomers.

TASK 9  : GET- Select a specific Customer entry by CustomerID.

TASK 11 : GET- Order history of a customer by CustomerID.















- SERVER_ADDRESS    `[IP Address of the machine]`
- SERVER_PORT       `[Port of the machine]`
- DB_USER           `[Database username]`
- DB_PASSWD         `[Database password]`
- DB_ADDR           `[IP address of the database]`
- DB_PORT           `[Port of the database]`
- DB_NAME           `[Name of the database]`

 
1. `resources/database.sql` this contains the SQL for generating the tables. In case you dont want to use the docker-compose file you can use this file to generate tables and insert the default data

# mocks generator
`./generate-mocks.sh`

# run unit tests
  `./run-tests.sh`
